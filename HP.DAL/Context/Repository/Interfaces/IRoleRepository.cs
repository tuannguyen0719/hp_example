﻿using HP.DAL.Context.Entities;

namespace HP.DAL.Context.Repository
{
    public interface IRoleRepository : IRepository<Role>
    {
    }
}
