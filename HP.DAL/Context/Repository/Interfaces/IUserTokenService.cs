﻿using HP.DAL.Context.Entities;
using System;
using System.Threading.Tasks;

namespace HP.DAL.Context.Repository
{
    public interface IUserTokenRepository : IRepository<UserToken>
    {
        Task<UserToken> Get(int userId, string token);

        Task<DateTime?> GetExpirationTime(int userId, string token, string ipAddress, string userAgent, int maxProvisionCount = 16);

        Task<bool> IsValidByExpirationTime(int userId, string token, string ipAddress, string userAgent, int minutesOffset = 0, int maxProvisionCount = 16);

        Task<int> GetId(int userId, string token, string ipAddress, string userAgent, int maxProvisionCount = 16);

        Task<bool> IsValid(int userId, string token, string ipAddress, string userAgent, int maxProvisionCount = 16);

        Task<int> DeleteByToken(int userId, string token);

        Task<int> DeleteExpired(int userId, int maxProvisionCount);

        Task<int> DeleteExpired(int maxProvisionCount);
    }
}