﻿using HP.DAL.Context.Entities;
using Microsoft.Extensions.Configuration;

namespace HP.DAL.Context.Repository
{
    public class FileInfoRepository : Repository<FileInfo>, IFileInfoRepository
    {
        public FileInfoRepository(IConfiguration configuration)
            : base(configuration)
        {
        }
    }
}
