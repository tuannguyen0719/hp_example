﻿using HP.DAL.Context.Entities;
using Microsoft.Extensions.Configuration;

namespace HP.DAL.Context.Repository
{
    public class UserLoginRepository : Repository<UserLogin>, IUserLoginRepository
    {
        public UserLoginRepository(IConfiguration configuration)
            : base(configuration)
        {
        }
    }
}