﻿using Dapper;
using HP.DAL.Context.Entities;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace HP.DAL.Context.Repository
{
    public enum LoginProviderType
    {
        Forms,
        Windows,
        PKI
    }

    public class UserRepository : Repository<User>, IUserRepository
    {
        public UserRepository(IConfiguration configuration)
            : base(configuration)
        {
        }

        public override Task<IEnumerable<User>> GetAll(QueryFlags flags = QueryFlags.None)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.QueryAsync<User>(@"
                        SELECT 
	                        u.*,
	                        (SELECT COALESCE(string_agg(uc.claim_value , ';'), 'Viewer')
	                            FROM admin.user_claim uc 
	                            WHERE uc.id_user = u.id_user 
	                            AND  uc.claim_type = 'http://schemas.microsoft.com/ws/2008/06/identity/claims/role') as roles
                        FROM admin.user u");
            }
        }

        public override Task<User> GetById(int id, QueryFlags flags = QueryFlags.None)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.QueryFirstOrDefaultAsync<User>(@"
                        SELECT 
	                        u.*,
	                        (SELECT COALESCE(string_agg(uc.claim_value , ';'), 'Viewer')
	                            FROM admin.user_claim uc 
	                            WHERE uc.id_user = u.id_user 
	                            AND  uc.claim_type = @roleType) as roles
                        FROM admin.user u where u.id_user = @id",
                new
                {
                    id,
                    roleType = "http://schemas.microsoft.com/ws/2008/06/identity/claims/role"
                });
            }
        }

        public Task<User> GetByLogin(string userName, string pwd)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.QueryFirstOrDefaultAsync<User>(@"
                        select 
                        u.*
                        from admin.user u
                        WHERE username = @userName AND pwd = @pwd",
                new
                {
                    userName,
                    pwd
                });
            }
        }

        public Task<User> GetByUserName(string userName)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.QueryFirstOrDefaultAsync<User>(@"
                        select 
                        u.*
                        from admin.user u
                        WHERE username = @userName",
                new
                {
                    userName
                });
            }
        }

        public Task<int> UpdateLockoutInfo(User userToUpdate)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.ExecuteAsync(@"
                        UPDATE admin.user
                        SET failed_login_count = @FailedLoginCount,
                        date_lockout_end = @LockoutEndDate
                        WHERE id_user = @Id",
                new
                {
                    userToUpdate.Id,
                    userToUpdate.FailedLoginCount,
                    userToUpdate.LockoutEndDate
                });
            }
        }

        public Task<UserLoginProvider> GetLoginProvider(int userId, LoginProviderType providerType)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                return dbConnection.QueryFirstOrDefaultAsync<UserLoginProvider>(@"
                        SELECT id_user_login_provider, id_user, provider_name, date_created
                        FROM admin.user_login_provider
                        WHERE id_user = @userId AND provider_name = @providerName",
                new
                {
                    userId,
                    providerName = Enum.GetName(typeof(LoginProviderType), providerType)
                });
            }
        }

        public Task<int> Insert(User userToCreate, IEnumerable<Claim> userClaims, LoginProviderType providerType)
        {
            return Task.Run<int>(() =>
            {
                int numberOfRowsAffected = 0;

                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();

                    using (IDbTransaction transaction = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted))
                    {
                        try
                        {
                            numberOfRowsAffected = dbConnection.Execute(@"
                            INSERT INTO admin.user(
	                            username, 
	                            password, 
	                            name_first, 
	                            name_middle_initial, 
	                            name_last, 
                                title,
	                            email, 
	                            phone,
	                            name_display,
                                is_approved,
                                date_approved,
                                approved_by)
                            VALUES(@UserName, @Password, @FirstName, @MiddleInitial, @LastName, @Title, @Email, @Phonenumber, @DisplayName, @IsApproved, @ApprovedDate, @ApprovedBy",
                                new
                                {
                                    userToCreate.UserName,
                                    userToCreate.Password,
                                    userToCreate.FirstName,
                                    userToCreate.MiddleInitial,
                                    userToCreate.LastName,
                                    userToCreate.Title,
                                    userToCreate.Email,
                                    userToCreate.Phonenumber,
                                    userToCreate.DisplayName,
                                    userToCreate.IsApproved,
                                    userToCreate.ApprovedDate,
                                    userToCreate.ApprovedBy
                                });


                            foreach (var uClaim in userClaims)
                            {
                                numberOfRowsAffected += dbConnection.Execute(@"
                                    INSERT INTO admin.user_claim(
	                                    id_user, 
	                                    claim_type, 
	                                    claim_value)
                                    SELECT id_user, @Type, @Value FROM admin.user WHERE username = @UserName",
                                        new
                                        {
                                            userToCreate.UserName,
                                            uClaim.Type,
                                            uClaim.Value,
                                        });
                            }


                            numberOfRowsAffected += dbConnection.Execute(@"
                                    INSERT INTO admin.user_login_provider(
	                                    id_user, 
	                                    provider_name)
                                    SELECT id_user, @loginProviderName FROM admin.user WHERE username = @UserName",
                                        new
                                        {
                                            userToCreate.UserName,
                                            loginProviderName = Enum.GetName(typeof(LoginProviderType), providerType)
                                        });

                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            numberOfRowsAffected = 0;
                            throw;
                        }
                    }
                }

                return numberOfRowsAffected;
            });
        }

        public Task<int> Update<T2>(int id, T2 userToUpdate, string semicolonDelimitedRoles) where T2 : class
        {
            //TODO: Move this function to a service.
            return Task.Run<int>(() =>
            {
                int numberOfRowsAffected = 0;

                using (IDbConnection dbConnection = Connection)
                {
                    dbConnection.Open();

                    using (IDbTransaction transaction = dbConnection.BeginTransaction(IsolationLevel.ReadCommitted))
                    {
                        try
                        {
                            numberOfRowsAffected = dbConnection.Execute(QueryBuilder.Update<User>(userToUpdate, "id_user=" + id), userToUpdate);

                            if (!string.IsNullOrWhiteSpace(semicolonDelimitedRoles))
                            {
                                numberOfRowsAffected += dbConnection.Execute(
                                     @"DELETE FROM admin.user_claim
                                     where id_user = @userId 
                                     AND claim_type = @claimType",
                                     new
                                     {
                                         id,
                                         claimType = ClaimTypes.Role
                                     });


                                var roles = semicolonDelimitedRoles.Split(';');


                                foreach (var claimValue in roles)
                                {
                                    numberOfRowsAffected += dbConnection.Execute(
                                        @"INSERT INTO admin.user_claim(
	                                    id_user, claim_type, claim_value)
	                                    VALUES (@userId, @claimType, @claimValue)",
                                        new
                                        {
                                            id,
                                            ClaimTypes.Role,
                                            claimValue
                                        });
                                }
                            }

                            transaction.Commit();
                        }
                        catch (Exception)
                        {
                            transaction.Rollback();
                            numberOfRowsAffected = 0;
                            throw;
                        }
                    }
                }

                return numberOfRowsAffected;
            });
        }

        public Task<IEnumerable<string>> GetEmailsByRoles(IEnumerable<string> roles)
        {
            using (IDbConnection dbConnection = Connection)
            {
                dbConnection.Open();

                dynamic parameters = new ExpandoObject();
                var expandoDict = parameters as IDictionary<string, object>;

                StringBuilder builder = new StringBuilder();
                int i = 0;

                foreach (var n in roles)
                {
                    builder.Append("@n" + i + ",");
                    expandoDict.Add("@n" + i, n);
                    i++;
                }

                builder.Length--;

                return dbConnection.QueryAsync<string>(@"
                        SELECT DISTINCT u.email
                        FROM admin.user u
                        JOIN admin.user_claim uc ON uc.id_user = u.id_user
                        WHERE uc.claim_type = 'http://schemas.microsoft.com/ws/2008/06/identity/claims/role'
                        AND uc.claim_value IN (" + builder.ToString() + ")", parameters as object);
            }
        }

        public Task<IEnumerable<string>> GetEmailsByRoles(params string[] roles)
        {
            return GetEmailsByRoles(roles.AsEnumerable());
        }
    }
}
