﻿namespace HP.DAL.Context.Entities
{
    //
    // Summary:
    //     Possible results from a sign in attempt
    public enum SignInStatus
    {
        //
        // Summary:
        //     Sign in was successful
        Success = 0,
        //
        // Summary:
        //     User is locked out
        LockedOut = 1,
        //
        // Summary:
        //     Sign in requires addition verification (i.e. two factor)
        RequiresVerification = 2,
        //
        // Summary:
        //     User is disabled
        Disabled = 3,
        //
        // Summary:
        //     User is not found
        NotFound = 4,
        //
        // Summary:
        //     Sign in requires addition approval from admin
        RequiresApproval = 5,
        //
        // Summary:
        //     Sign in failed
        Failure = 13
    }

    public enum SignInStatusId
    {
        //
        // Summary:
        //     Sign in was successful
        Success = 1,
        //
        // Summary:
        //     User is locked out
        LockedOut = 2,
        //
        // Summary:
        //     Sign in requires addition verification (i.e. two factor)
        RequiresVerification = 3,
        //
        // Summary:
        //     User is disabled
        Disabled = 4,
        //
        // Summary:
        //     User is not found
        NotFound = 5,
        //
        // Summary:
        //     Sign in requires addition approval from admin
        RequiresApproval = 6,
        //
        // Summary:
        //     Sign in failed
        Failure = 7
    }
}
