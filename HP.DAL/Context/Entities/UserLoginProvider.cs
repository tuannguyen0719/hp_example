﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace HP.DAL.Context.Entities
{
    [Table("admin.user_login_provider")]
    public class UserLoginProvider : BaseEntity
    {
        [Key]
        [Column("id_user_login_provider")]
        public int Id { get; set; }

        [Required]
        [Column("id_user")]
        public int UserId { get; set; }

        [Required]
        [Column("provider_name")]
        public string ProviderName { get; set; }

        [Required]
        [Column("date_created")]
        public DateTime CreatedDate { get; set; }
    }
}
