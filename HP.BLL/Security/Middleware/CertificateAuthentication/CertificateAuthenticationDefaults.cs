﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HP.BLL.Security.Middleware.CertificateAuthentication
{
    public static class CertificateAuthenticationDefaults
    {
        /// <summary>
        /// Default value for AuthenticationScheme property in the CertificateAuthenticationOptions
        /// </summary>
        public const string AuthenticationScheme = "Certificate";
    }
}
