﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Text.Encodings.Web;
using System.Threading.Tasks;

namespace HP.BLL.Security.Middleware.CertificateAuthentication
{
    internal class CertificateAuthenticationHandler : AuthenticationHandler<CertficateAuthenticationOptions>
    {
        public CertificateAuthenticationHandler(IOptionsMonitor<CertficateAuthenticationOptions> options, ILoggerFactory logger, UrlEncoder encoder, IDataProtectionProvider dataProtection, ISystemClock clock)
            : base(options, logger, encoder, clock)
        { }

        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var certificate = Context.Connection.ClientCertificate;
            if (certificate != null && certificate.Verify() && (Options.AllowedIssuers?.Any(i => i == certificate.GetNameInfo(X509NameType.DnsName, true)) ?? true))
            {
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.AuthenticationMethod, Options.Challenge),
                    new Claim(ClaimTypes.AuthorizationDecision, certificate.GetNameInfo(X509NameType.DnsName, true)),
                    new Claim(ClaimTypes.NameIdentifier, certificate.GetNameInfo(X509NameType.UpnName, false)),
                    new Claim(ClaimTypes.Name, certificate.GetNameInfo(X509NameType.DnsName, false) ?? ""),
                    new Claim(ClaimTypes.Email, certificate.GetNameInfo(X509NameType.EmailName, false) ?? ""),
                    new Claim(ClaimTypes.Expiration, certificate.GetExpirationDateString() ?? "")
                };


                if (Options.OnCreateClaims != null)
                {
                    claims = await Options.OnCreateClaims(certificate, claims);
                }


                if (claims?.Count > 0)
                {
                    var userIdentity = new ClaimsIdentity(claims, Options.Challenge);
                    var userPrincipal = new ClaimsPrincipal(userIdentity);
                    var ticket = new AuthenticationTicket(userPrincipal, new AuthenticationProperties(), Options.Challenge);
                    return AuthenticateResult.Success(ticket);
                }
            }
            else if (certificate != null)
            {
                string message = "Invalid Certificate: subject->" + certificate.Subject
                    + " Issuer->" + certificate.GetNameInfo(X509NameType.DnsName, true) ?? ""
                    + " SAN->" + certificate.GetNameInfo(X509NameType.DnsName, false) ?? ""
                    + " Email->" + certificate.GetNameInfo(X509NameType.EmailName, false) ?? ""
                    + " ExpDate->" + certificate.GetExpirationDateString()
                    + " IpAddress->" + Context.Connection.RemoteIpAddress?.ToString() ?? "";

                new Exception("Certificate is invalid: " + message).Log();
            }

            return AuthenticateResult.NoResult();
        }
    }
}
