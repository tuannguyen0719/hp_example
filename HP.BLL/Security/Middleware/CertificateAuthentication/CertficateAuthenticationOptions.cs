﻿using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace HP.BLL.Security.Middleware.CertificateAuthentication
{
    public class CertficateAuthenticationOptions : AuthenticationSchemeOptions
    {
        /// <summary>
        /// Gets or sets the challenge to put in the "WWW-Authenticate" header.
        /// </summary>
        public string Challenge { get; set; } = CertificateAuthenticationDefaults.AuthenticationScheme;

        public IEnumerable<string> AllowedIssuers { get; set; }

        public Func<X509Certificate2, List<Claim>, Task<List<Claim>>> OnCreateClaims { get; set; }
    }
}
