﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;
using HP.BLL.Security.Middleware.WindowsAuthentication;

namespace Microsoft.AspNetCore.Authentication
{
    public static class WindowsAuthenticationExtensions
    {
        public static AuthenticationBuilder AddWindowsAuthentication(this AuthenticationBuilder builder, Action<WindowsAuthenticationOptions> configureOptions)
        {
            builder.Services.TryAddEnumerable(ServiceDescriptor.Singleton<IPostConfigureOptions<WindowsAuthenticationOptions>, WindowsAuthenticationPostConfigureOptions>());
            return builder.AddScheme<WindowsAuthenticationOptions, WindowsAuthenticationHandler>(WindowsAuthenticationDefaults.AuthenticationScheme, "Windows Authentication", configureOptions);
        }
        public static AuthenticationBuilder AddWindowsAuthentication(this AuthenticationBuilder builder)
        {
            builder.Services.TryAddEnumerable(ServiceDescriptor.Singleton<IPostConfigureOptions<WindowsAuthenticationOptions>, WindowsAuthenticationPostConfigureOptions>());
            return builder.AddScheme<WindowsAuthenticationOptions, WindowsAuthenticationHandler>(WindowsAuthenticationDefaults.AuthenticationScheme, "Windows Authentication", options => { });
        }
    }
}
