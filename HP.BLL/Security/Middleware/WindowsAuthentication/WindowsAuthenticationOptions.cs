﻿using Microsoft.AspNetCore.Authentication;
using System;
using System.Collections.Generic;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;

namespace HP.BLL.Security.Middleware.WindowsAuthentication
{
    public class WindowsAuthenticationOptions : AuthenticationSchemeOptions
    {
        /// <summary>
        /// Gets or sets the challenge to put in the "WWW-Authenticate" header.
        /// </summary>
        public string Challenge { get; set; } = WindowsAuthenticationDefaults.AuthenticationScheme;

        public Func<WindowsPrincipal, Task<List<Claim>>> OnCreateClaims { get; set; }
    }
}
