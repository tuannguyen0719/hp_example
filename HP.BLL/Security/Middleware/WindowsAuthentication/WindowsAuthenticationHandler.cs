﻿using System;
using System.Security.Claims;
using System.Globalization;
using System.Security.Principal;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using Microsoft.Extensions.Options;
using Microsoft.Extensions.Logging;
using System.Text.Encodings.Web;
using Microsoft.AspNetCore.DataProtection;
using System.Collections.Generic;

namespace HP.BLL.Security.Middleware.WindowsAuthentication
{
    internal class WindowsAuthenticationHandler : AuthenticationHandler<WindowsAuthenticationOptions>
    {
        private const string MSAspNetCoreWinAuthToken = "MS-ASPNETCORE-WINAUTHTOKEN";


        public WindowsAuthenticationHandler(IOptionsMonitor<WindowsAuthenticationOptions> options, ILoggerFactory logger, UrlEncoder encoder, IDataProtectionProvider dataProtection, ISystemClock clock)
            : base(options, logger, encoder, clock)
        { }


        protected override async Task<AuthenticateResult> HandleAuthenticateAsync()
        {
            var user = GetUser();

            if (user != null)
            {
                var claims = new List<Claim>();


                if (Options.OnCreateClaims != null)
                {
                    claims = await Options.OnCreateClaims(user);
                }
                else
                {
                    claims = new List<Claim>(user.Claims);
                }


                if (claims?.Count > 0)
                {
                    var userIdentity = new ClaimsIdentity(claims, Options.Challenge);
                    var userPrincipal = new ClaimsPrincipal(userIdentity);
                    var ticket = new AuthenticationTicket(userPrincipal, Options.Challenge);
                    return AuthenticateResult.Success(ticket);
                }
            }

            return AuthenticateResult.NoResult();
        }

        private WindowsPrincipal GetUser()
        {
            WindowsPrincipal user = null;

            var tokenHeader = Context.Request.Headers[MSAspNetCoreWinAuthToken];

            int hexHandle;
            if (!StringValues.IsNullOrEmpty(tokenHeader)
                && int.TryParse(tokenHeader, NumberStyles.HexNumber, CultureInfo.InvariantCulture, out hexHandle))
            {
                // Always create the identity if the handle exists, we need to dispose it so it does not leak.
                var handle = new IntPtr(hexHandle);
                var winIdentity = new WindowsIdentity(handle);

                // WindowsIdentity just duplicated the handle so we need to close the original.
                NativeMethods.CloseHandle(handle);

                Context.Response.RegisterForDispose(winIdentity);

                user = new WindowsPrincipal(winIdentity);
            }

            return user;
        }

        protected override Task HandleChallengeAsync(AuthenticationProperties properties)
        {
            // We would normally set the www-authenticate header here, but IIS does that for us.
            Context.Response.StatusCode = 401;
            return Task.CompletedTask;
        }

        protected override Task HandleForbiddenAsync(AuthenticationProperties properties)
        {
            Context.Response.StatusCode = 403;
            return Task.CompletedTask;
        }
    }
}
