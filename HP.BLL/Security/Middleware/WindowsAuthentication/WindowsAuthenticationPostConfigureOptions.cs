﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Text;

namespace HP.BLL.Security.Middleware.WindowsAuthentication
{
    public class WindowsAuthenticationPostConfigureOptions : IPostConfigureOptions<WindowsAuthenticationOptions>
    {
        /// <summary>
        /// Invoked to post configure a CertficateAuthenticationOptions instance.
        /// </summary>
        /// <param name="name">The name of the options instance being configured.</param>
        /// <param name="options">The options instance to configure.</param>
        public void PostConfigure(string name, WindowsAuthenticationOptions options)
        {

        }
    }
}
