﻿using HP.DAL.Context.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Primitives;
using System;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HP.BLL.Security
{
    public class CheckAuthRequirement : IAuthorizationRequirement
    {
        public string IpClaimType { get; set; }

        public string UserAgentClaimType { get; set; }

        public string AuthenticationType { get; set; }
    }

    public class CheckAuthHandler : AuthorizationHandler<CheckAuthRequirement>
    {
        public CheckAuthHandler(IHttpContextAccessor httpContextAccessor, IUserTokenRepository userTokenService, JwtSettings jwtSettings)
        {
            HttpContextAccessor = httpContextAccessor ?? throw new ArgumentNullException(nameof(httpContextAccessor));
            _svcUserToken = userTokenService ?? throw new ArgumentNullException(nameof(userTokenService));
            _JwtSettings = jwtSettings ?? throw new ArgumentNullException(nameof(jwtSettings));
        }

        IHttpContextAccessor HttpContextAccessor { get; }
        HttpContext HttpContext => HttpContextAccessor.HttpContext;
        IUserTokenRepository _svcUserToken { get; }
        JwtSettings _JwtSettings { get; }


        protected override async Task HandleRequirementAsync(AuthorizationHandlerContext context, CheckAuthRequirement requirement)
        {
            if (HttpContext.User.Identity.IsAuthenticated
                && HttpContext.User.Identity.AuthenticationType == requirement.AuthenticationType)
            {
                if (_JwtSettings.IsDatabasePersistent)
                {
                    string accesToken = HttpContext.Request.Headers["Authorization"].ToString().Replace("Bearer ", "");
                    string clientIp = HttpContext.Connection.RemoteIpAddress?.ToString();
                    string userAgent = HttpContext.Request.Headers.TryGetValue("User-Agent", out StringValues userAgentValue) ? userAgentValue.ToString() : null;

                    if (string.IsNullOrWhiteSpace(clientIp))
                    {
                        new Exception("Missing IP address for: name=>" + HttpContext.User.Identity.Name).Log();

                        context.Fail();
                    }
                    else if (string.IsNullOrWhiteSpace(userAgent))
                    {
                        new Exception("Missing User-Agent for: name=>" + HttpContext.User.Identity.Name).Log();

                        context.Fail();
                    }
                    else if (!await _svcUserToken.IsValidByExpirationTime(
                           HttpContext.User.GetId(),
                           accesToken,
                           clientIp,
                           userAgent,
                           -_JwtSettings.ExpiresInMinutes
                           ))
                    {
                        context.Fail();
                    }
                }
                else
                {
                    if (_JwtSettings.IsUseIpAddressValidation && !string.IsNullOrWhiteSpace(requirement.IpClaimType))
                    {
                        string clientIp = HttpContext.Connection.RemoteIpAddress?.ToString();

                        if (!string.IsNullOrWhiteSpace(clientIp))
                        {
                            Claim ipClaim = context.User.FindFirst(claim => claim.Type == requirement.IpClaimType);

                            if (ipClaim == null || ipClaim.Value != clientIp)
                            {
                                new Exception("Unauthorized IP address for: ip=>" + clientIp + " name=>" + HttpContext.User.Identity.Name).Log();

                                context.Fail();
                            }
                        }
                        else
                        {
                            new Exception("Missing IP address for: name=>" + HttpContext.User.Identity.Name).Log();

                            context.Fail();
                        }
                    }

                    if (!context.HasFailed && _JwtSettings.IsUseUserAgentValidation && !string.IsNullOrWhiteSpace(requirement.UserAgentClaimType))
                    {
                        if (HttpContext.Request.Headers.TryGetValue("User-Agent", out StringValues userAgentValue))
                        {
                            Claim uaClaim = context.User.FindFirst(claim => claim.Type == requirement.UserAgentClaimType);

                            if (uaClaim == null || uaClaim.Value != userAgentValue.ToString())
                            {
                                new Exception("Unauthorized User-Agent for: name=>" + HttpContext.User.Identity.Name).Log();

                                context.Fail();
                            }
                        }
                        else
                        {
                            new Exception("Missing User-Agent for: name=>" + HttpContext.User.Identity.Name).Log();

                            context.Fail();
                        }
                    }
                }
            }

            if (!context.HasFailed)
            {
                context.Succeed(requirement);
            }

            //return Task.CompletedTask;
        }
    }
}
