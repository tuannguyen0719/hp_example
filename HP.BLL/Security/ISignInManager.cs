﻿using HP.DAL.Context.Entities;
using HP.DAL.Context.Repository;
using Microsoft.IdentityModel.Tokens;
using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;

namespace HP.BLL.Security
{
    public interface ISignInManager
    {
        Task<SignInResult> PasswordSignInAsync(
            string userName,
            string password,
            LoginProviderType providerType);

        Task<SignInResult> SignInAsync(
             string userName,
             LoginProviderType providerType,
             RefreshTokenModel refreshModel);

        Task<SignInResult> SignInAsync(
            string userName,
            LoginProviderType providerType);

        void InvalidateUser(int userId);

        bool IsUserValid(int userId);

        Task<IEnumerable<Claim>> GenerateClaimsForUser(User userData, LoginProviderType type);

        ClaimsPrincipal ValidateToken(string token, out SecurityToken validatedToken);

        Task<SignInResult> SignInWithRefreshToken(RefreshTokenModel model);
    }
}
