﻿using System;
using System.Collections.Generic;
using System.Text;

namespace System
{
    public static class StringExtensions
    {
        public static T Convert<T>(this string value)
        {
            T obj = default(T);

            try
            {
                if (!string.IsNullOrWhiteSpace(value))
                {

                    Type tType = typeof(T);

                    if (tType.IsEnum)
                    {
                        obj = (T)Enum.Parse(tType, value);
                    }
                    else
                    {
                        Type nullableType = Nullable.GetUnderlyingType(tType);

                        if (nullableType != null)
                        {
                            tType = nullableType;
                        }

                        obj = (T)System.Convert.ChangeType(value, tType);
                    }
                }
            }
            catch (Exception ex)
            {
                ex.Log();
            }

            return obj;
        }
    }
}
