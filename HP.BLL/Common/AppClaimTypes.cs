﻿using System;
using System.Collections.Generic;
using System.Text;

namespace HP.BLL.Common
{
    public static class AppClaimTypes
    {
        public const string AppName = "hp";

        public const string MiddleInitial = "http://" + AppName + "/2018/01/identity/claims/middleinitial";       
        public const string DisplayName = "http://" + AppName + "/2018/01/identity/claims/displayname";
        public const string IpAddress = "http://" + AppName + "/2018/01/identity/claims/ipaddress";
        public const string UserAgent = "http://" + AppName + "/2018/01/identity/claims/useragent";
        public const string Description = "http://" + AppName + "/2018/01/identity/claims/description";
        public const string LoginProviderType = "http://" + AppName + "/2018/01/identity/claims/loginprovidertype";
    }
}
