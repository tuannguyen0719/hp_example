﻿


I.) IISExpress Authentication Configuration

!!!!!REMARK!!!!!!
When running the app with Visual Studio, section "unlocks" have to be done for IISExpress inside your project's ".vs\config\applicationhost.config" file.
!!!!!END REMARK!!!!!!

!!!!!REMARK!!!!!!
Your application must only have anonymous authentication enabled at all times.
!!!!!END REMARK!!!!!!

1.) Unlock security sections:
	a.) Navigate to your project's ".vs\config\applicationhost.config" file.
	b.) Under the "security" section group set the "overrideModeDefault" property to "Allow" for:
		"access", "anonymousAuthentication", "clientCertificateMappingAuthentication", "iisClientCertificateMappingAuthentication", and "windowsAuthentication"

		e.g:

		<sectionGroup name="security">
			<section name="access" overrideModeDefault="Allow" />
			<section name="applicationDependencies" overrideModeDefault="Deny" />
			<sectionGroup name="authentication">
			  <section name="anonymousAuthentication" overrideModeDefault="Allow" />
			  <section name="basicAuthentication" overrideModeDefault="Deny" />
			  <section name="clientCertificateMappingAuthentication" overrideModeDefault="Allow" />
			  <section name="digestAuthentication" overrideModeDefault="Deny" />
			  <section name="iisClientCertificateMappingAuthentication" overrideModeDefault="Allow" />
			  <section name="windowsAuthentication" overrideModeDefault="Allow" />
			</sectionGroup>
			<section name="authorization" overrideModeDefault="Allow" />
			<section name="ipSecurity" overrideModeDefault="Deny" />
			<section name="dynamicIpSecurity" overrideModeDefault="Deny" />
			<section name="isapiCgiRestriction" allowDefinition="AppHostOnly" overrideModeDefault="Deny" />
			<section name="requestFiltering" overrideModeDefault="Allow" />
		</sectionGroup>




II.) IIS Deployment Authentication Configuration:

!!!!!REMARK!!!!!!
Your web site or application must only have anonymous authentication enabled at all times.
!!!!!END REMARK!!!!!!

1.) Windows Authentication:

	a.) Enable Windows Feature Delegation to be Read/Write:
		a.1) At the server level, open "Feature Delegation".
		a.2) "Right-click" the "Authentication - Windows" line and then click "Read/Write".

	b.) Unlock security sections:
		b.1) At the server level, open "Configuration Editor".
		b.2) Using the top "Section" drop-down, expand the system.webServer/security/authentication nodes.
		b.3) Select the "anonymousAuthentication" node.
		b.4) Under the top right "Actions" menu click the "Unlock Section" label.
		b.5) Repeat step b.2 and do the same for the "windowsAuthentication" node.
	
2.) Client Certificate Authentication:

	2a.) Unlock security sections:
		a.1) At the server level, open "Configuration Editor".
		a.2) Using the top "Section" drop-down, expand the system.webServer/security/authentication section.
		a.3) Select the "clientCertificateMappingAuthentication" node.
		a.4) Under the top right "Actions" menu click the "Unlock Section" label.
		a.5) Repeat step a.2 and do the same for the "iisClientCertificateMappingAuthentication" node.
		a.5) Repeat step a.2 but this time under the "system.webServer/security" section and do the same for the "access" node.
	 

!!!!!REMARK!!!!!!
Don't forget to enable HTTPS by "requiring" SSL for your web site or application
!!!!!END REMARK!!!!!!