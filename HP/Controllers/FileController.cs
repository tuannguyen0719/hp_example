﻿using HP.BLL.Security;
using HP.DAL.Context.Entities;
using HP.DAL.Context.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.AspNetCore.WebUtilities;
using Microsoft.Extensions.Configuration;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using FileInfo = HP.DAL.Context.Entities.FileInfo;

namespace HP.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FileController : ControllerBase
    {
        private readonly IFileInfoRepository _svcFileInfo;
        private readonly IConfiguration _configuration;
        private readonly ISignInManager _mgrSignIn;

        // Get the default form options so that we can use them to set the default limits for
        // request body data
        private static readonly FormOptions _defaultFormOptions = new FormOptions();


        public FileController(IConfiguration configuration, IFileInfoRepository fileInfoService, ISignInManager signInManager)
        {
            _configuration = configuration;
            _svcFileInfo = fileInfoService;
            _mgrSignIn = signInManager;
        }


        [HttpGet]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status206PartialContent)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<Stream>> Get(int id, string token)
        {
            try
            {
                //Get file info from database
                var fileResult = await _svcFileInfo.GetById(id);

                if (fileResult == null)
                {
                    return NotFound();
                }


                //Load the file stream
                Stream stream = GetStream(fileResult.Name, fileResult.FileCategoryId);
                if (stream == null)
                    return NotFound();
                Response.RegisterForDispose(stream);


                //Set contentType                
                new FileExtensionContentTypeProvider().TryGetContentType(fileResult.Name, out string contentType);


                //Set headers
                Response.Headers.Add("Content-Disposition", new System.Net.Mime.ContentDisposition
                {
                    FileName = fileResult.Name,
                    Inline = false,
                    CreationDate = fileResult.CreatedDate,
                    Size = stream.Length
                }.ToString());

                Response.Headers.Add("X-Content-Type-Options", "nosniff");


                return File(stream, contentType ?? "application/octet-stream", fileResult.Name, true);
            }
            catch (Exception ex)
            {
                ex.Log();

                if (ex is DirectoryNotFoundException || ex is FileNotFoundException)
                {
                    return NotFound();
                }
                else
                {
                    return StatusCode(StatusCodes.Status500InternalServerError, ex);
                }
            }
        }

        [HttpGet("Info/Latest")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IEnumerable<IEnumerable<FileInfo>>> GetLatestTopTenInfo()
        {
            //TODO
            return await Task.FromResult(Enumerable.Empty<IEnumerable<FileInfo>>());
        }

        [HttpGet("Info/{queryType:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IEnumerable<Dictionary<string, object>>> GetInfo(FileQueryType queryType)
        {
            //TODO
            return await Task.FromResult(Enumerable.Empty<Dictionary<string, object>>());
        }

        [HttpGet("Info/{id:int}/{categoryType:int}/{queryType:int}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IEnumerable<FileInfo>> GetInfoByCategory(int id, FileCategoryType categoryType, FileQueryType queryType)
        {
            //TODO
            return await Task.FromResult(Enumerable.Empty<FileInfo>());
        }

        [HttpPost("{key}/{category}/{isOverwrite?}")]
        [Authorize(Roles = "Administrator")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        [DisableFormValueModelBinding]
        public async Task<ActionResult> Post([FromRoute]int key, [FromRoute] FileCategoryType category, [FromRoute] bool isOverwrite = false)
        {
            if (key == 0)
            {
                return BadRequest();
            }

            try
            {
                var headers = Request.GetTypedHeaders();

                if (MultipartRequestHelper.IsMultipartContentType(Request.ContentType))
                {
                    var boundary = MultipartRequestHelper.GetBoundary(
                                    MediaTypeHeaderValue.Parse(Request.ContentType),
                                    _defaultFormOptions.MultipartBoundaryLengthLimit);


                    var reader = new MultipartReader(boundary, HttpContext.Request.Body);
                    var section = await reader.ReadNextSectionAsync();


                    if (section == null)
                    {
                        return BadRequest("Error: Incorrect or missing boundary!");
                    }


                    while (section != null)
                    {
                        var hasContentDispositionHeader = ContentDispositionHeaderValue.TryParse(section.ContentDisposition, out ContentDispositionHeaderValue contentDisposition);

                        if (hasContentDispositionHeader)
                        {
                            if (MultipartRequestHelper.HasFileContentDisposition(contentDisposition))
                            {
                                string fileName = Path.GetFileName(contentDisposition.FileName.Value);

                                if (!string.IsNullOrWhiteSpace(fileName))
                                {
                                    await SaveStream(key, category, fileName, section.Body, 0, isOverwrite);
                                }
                                else
                                {
                                    return BadRequest("Error: Missing Content-Disposition file name!");
                                }
                            }
                        }


                        // Drains any remaining section body that has not been consumed and
                        // reads the headers for the next section.
                        section = await reader.ReadNextSectionAsync();
                    }
                }
                else if (headers.ContentType.MediaType == "application/octet-stream")
                {
                    string fileName = null;

                    if (headers.ContentDisposition != null && headers.ContentDisposition.FileName != null)
                    {
                        fileName = headers.ContentDisposition.FileName.Value?.Trim('\"');
                    }

                    if (!string.IsNullOrWhiteSpace(fileName))
                    {
                        //Set offset from range header.
                        long offset = headers.ContentRange != null ? headers.ContentRange.From ?? 0 : 0;

                        await SaveStream(key, category, fileName, Request.Body, offset, isOverwrite);
                    }
                    else
                    {
                        return BadRequest("Error: Missing Content-Disposition file name!");
                    }
                }
                else
                {
                    return StatusCode(StatusCodes.Status415UnsupportedMediaType);
                }

                return Ok();
            }
            catch (Exception ex)
            {
                ex.Log();

                return StatusCode(StatusCodes.Status500InternalServerError);
            }
        }


        [HttpDelete("{id}")]
        [Authorize(Roles = "Administrator")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status500InternalServerError)]
        public async Task<ActionResult<bool>> Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest(ModelState);
            }
            else
            {
                try
                {
                    //Get file info from database
                    var fileResult = await _svcFileInfo.GetById(id);

                    if (fileResult == null)
                    {
                        return NotFound();
                    }


                    var deleteResult = await _svcFileInfo.Delete(id);

                    if (deleteResult == 0)
                    {
                        return NotFound();
                    }


                    //Delete the file
                    DeleteFile(fileResult.Name, fileResult.FileCategoryId);


                    return Ok();
                }
                catch (Exception ex)
                {
                    ex.Log();

                    return StatusCode(StatusCodes.Status500InternalServerError);
                }
            }
        }


        #region Helpers

        private string GetFileRepositoryPath()
        {

            return _configuration.GetValue<string>("FileRepositoryPath");
        }

        private string GetPath(string fileName, FileCategoryType category)
        {
            string path = Path.Combine(GetFileRepositoryPath(), Enum.GetName(typeof(FileCategoryType), category));

            return Path.Combine(path, fileName);
        }

        private Stream GetStream(string fileName, FileCategoryType category)
        {
            return System.IO.File.Open(GetPath(fileName, category), FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
        }

        private async Task SaveStream(int key, FileCategoryType category, string fileName, Stream inputStream, long offset = 0, bool isOverwrite = false)
        {
            string path = Path.Combine(GetFileRepositoryPath(), Enum.GetName(typeof(FileCategoryType), category));

            if (!System.IO.Directory.Exists(path))
            {
                System.IO.Directory.CreateDirectory(path);
            }


            path = Path.Combine(path, fileName);


            //Change filename if exists
            if (!isOverwrite && offset == 0)
            {
                int i = 1;
                string newPath = path;

                while (System.IO.File.Exists(newPath))
                {
                    newPath = path.Replace(fileName, fileName.Insert(fileName.LastIndexOf('.'), " (" + i + ")"));
                    i++;
                }

                path = newPath;
            }

            //Save file stream
            using (var fileStream = System.IO.File.Open(path, FileMode.OpenOrCreate, FileAccess.Write, FileShare.ReadWrite))
            {
                fileStream.Seek(offset, SeekOrigin.Begin);

                using (inputStream)
                {
                    await inputStream.CopyToAsync(fileStream, 1024 * 100);
                }
            }

            //Create database record
            if (offset == 0)
            {
                int userId = User.GetId();

                var createResult = await _svcFileInfo.Insert(new FileInfo { Key = key, Name = Path.GetFileName(path), FileCategoryId = category, ModifiedBy = userId, CreatedBy = userId });

                if (createResult == 0)
                {
                    System.IO.File.Delete(path);
                    throw new Exception("Failed to insert the file!");
                }
            }
        }

        private void DeleteFile(string fileName, FileCategoryType category)
        {
            System.IO.File.Delete(GetPath(fileName, category));
        }

        #endregion
    }



    public static class MultipartRequestHelper
    {
        // Content-Type: multipart/form-data; boundary="----WebKitFormBoundarymx2fSWqWSd0OxQqq"
        // The spec says 70 characters is a reasonable limit.
        public static string GetBoundary(MediaTypeHeaderValue contentType, int lengthLimit)
        {
            var boundary = HeaderUtilities.RemoveQuotes(contentType.Boundary).Value;
            if (string.IsNullOrWhiteSpace(boundary))
            {
                throw new InvalidDataException("Missing content-type boundary.");
            }

            if (boundary.Length > lengthLimit)
            {
                throw new InvalidDataException(
                    $"Multipart boundary length limit {lengthLimit} exceeded.");
            }

            return boundary;
        }

        public static bool IsMultipartContentType(string contentType)
        {
            return !string.IsNullOrEmpty(contentType)
                   && contentType.IndexOf("multipart/", StringComparison.OrdinalIgnoreCase) >= 0;
        }

        public static bool HasFormDataContentDisposition(ContentDispositionHeaderValue contentDisposition)
        {
            // Content-Disposition: form-data; name="key";
            return contentDisposition != null
                   && contentDisposition.DispositionType.Equals("form-data")
                   && string.IsNullOrEmpty(contentDisposition.FileName.Value)
                   && string.IsNullOrEmpty(contentDisposition.FileNameStar.Value);
        }

        public static bool HasFileContentDisposition(ContentDispositionHeaderValue contentDisposition)
        {
            // Content-Disposition: form-data; name="myfile1"; filename="Misc 002.jpg"
            return contentDisposition != null
                   && contentDisposition.DispositionType.Equals("form-data")
                   && (!string.IsNullOrEmpty(contentDisposition.FileName.Value)
                       || !string.IsNullOrEmpty(contentDisposition.FileNameStar.Value));
        }

        public static Encoding GetEncoding(MultipartSection section)
        {
            MediaTypeHeaderValue mediaType;
            var hasMediaTypeHeader = MediaTypeHeaderValue.TryParse(section.ContentType, out mediaType);
            // UTF-7 is insecure and should not be honored. UTF-8 will succeed in 
            // most cases.
            if (!hasMediaTypeHeader || Encoding.UTF7.Equals(mediaType.Encoding))
            {
                return Encoding.UTF8;
            }
            return mediaType.Encoding;
        }
    }


    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method)]
    public class DisableFormValueModelBindingAttribute : Attribute, IResourceFilter
    {
        public void OnResourceExecuting(ResourceExecutingContext context)
        {
            var factories = context.ValueProviderFactories;
            factories.RemoveType<FormValueProviderFactory>();
            factories.RemoveType<JQueryFormValueProviderFactory>();
        }

        public void OnResourceExecuted(ResourceExecutedContext context)
        {
        }
    }
}
